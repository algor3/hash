/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.


/**
 *
 * @author Pla
 */
public class Linear<key, value> {

    private int index = 100;
    private value[] val = (value[]) new Object[index];
    private key[] keys = (key[]) new Object[index];

    private int hash(key key) {
        return (key.hashCode() & 0x7FFFFFFF) % index;
    }

    public void put(key key, value value) {
        int i;
        for (i = hash(key); keys[i] != null; i = (i + 1) % index) {
            if (keys[i].equals(key)) {
                break;
            }
        }
        keys[i] = key;
        val[i] = value;
    }

    public value get(key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % index) {
            if (keys[i].equals(key)) {
                return val[i];
            }
        }
        return null;
    }
    
    public value remove(key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % index) {
            if (keys[i].equals(key)) {
                keys[i] = null;
                val[i] = null;
            }
        }
        return null;
    }

    
}
